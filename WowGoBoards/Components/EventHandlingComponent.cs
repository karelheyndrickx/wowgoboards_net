﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Core.Services.Implement;
using Umbraco.Web;

namespace MyProject.Components
{
    [RuntimeLevel(MinLevel = RuntimeLevel.Run)]
    public class MyComposer : IUserComposer
    {
        public void Compose(Composition composition) {

            composition.Components().Append<MyComponent>();
        }
    }

    public class MyComponent : IComponent
    {
        public void Initialize() {

            List<IContent> wereSaved = new List<IContent>();
            /*
            List<string> toBeUpdatedList = new List<string>();


            ContentService.Saving += (contentService, e) => {
                foreach (IContent content in e.SavedEntities.Where(c => c.ContentType.Alias.InvariantEquals("Product"))) {
                    if (!wereSaved.Contains(content)) {
                        wereSaved.Add(content);
                    }
                   
                }
            };


            ContentService.Publishing += (contentService, e) => {
                foreach (IContent content in e.PublishedEntities.Where(c => c.ContentType.Alias.InvariantEquals("Product"))) {
                    if (wereSaved.Contains(content)) {
                        toBeUpdatedList.Add("/products/" + content.Id);
                        wereSaved.Remove(content);
                    }
                }

                var toBeUpdatedArray = toBeUpdatedList.ToArray();
                var toBeUpdatedJson = JsonConvert.SerializeObject(toBeUpdatedArray);
                System.IO.File.WriteAllText(@"D:\projects\wowgoboards_nuxt\extra_static_routes\extra_routes.json", toBeUpdatedJson);

                //Clear the list afterwards
                toBeUpdatedList = new List<string>();


                var generateBatch = HttpContext.Current.Server.MapPath("~/Content/GenerateStaticPagesBetter.bat");
                ProcessStartInfo generateProcessInfo = new ProcessStartInfo(generateBatch);
                generateProcessInfo.CreateNoWindow = false;
                generateProcessInfo.RedirectStandardError = false;
                generateProcessInfo.RedirectStandardOutput = false;
                generateProcessInfo.UseShellExecute = true;
                Process generateProcess = Process.Start(generateProcessInfo);

                generateProcess.Close();

            };
            */

            var batFileName = HttpContext.Current.Server.MapPath("~/Content/StartNuxtServer.bat");
            ProcessStartInfo startInfo = new ProcessStartInfo(batFileName);
            startInfo.CreateNoWindow = false;
            startInfo.RedirectStandardError = false;
            startInfo.RedirectStandardOutput = false;
            startInfo.UseShellExecute = true;

            Console.WriteLine(startInfo.WorkingDirectory);
            Process process = Process.Start(startInfo);
            process.Close();


        }

        public void Terminate() {
        }

    }



}
