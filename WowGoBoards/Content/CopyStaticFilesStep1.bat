:: Step 1 of this copy batch file copies all the generated static files to a different folder called unremovable. 
:: Step 2 will then run after the generate command of the nuxt server has been called. 
:: Step 2 will copy the files that don't yet exist in the dist file from the unremovable folder back into the dist folde

d: 

cd \projects\wowgoboards_nuxt 

mkdir unremovable

xcopy dist unremovable /S /Y /D

