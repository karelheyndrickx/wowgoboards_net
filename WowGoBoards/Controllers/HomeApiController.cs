﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using WowGoBoards.Business.Mappers;
using WowGoBoards.Models;

namespace WowGoBoards.Controllers
{
    public class HomeApiController : UmbracoApiController
    {       
        public Home GetHomeInfo() {

            var root = Umbraco.ContentAtRoot().First();

            HomeMapper _mapper = new HomeMapper();
            return _mapper.MapHome(root);

           

        }        

    }
}
