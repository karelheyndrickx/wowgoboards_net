﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using WowGoBoards.Business.Mappers;
using WowGoBoards.Models;

namespace WowGoBoards.Controllers
{
    public class ProductsApiController : UmbracoApiController
    {

        public IEnumerable<Product> GetAllProducts() {
            return getProductsFromUmbraco();
        }

        public IEnumerable<Product> GetProductsInShort() {
            return getProductsFromUmbraco().Select(p => new Product {
                Id = p.Id,
                ProductName = p.ProductName,
                Price = p.Price,
                Description = p.Description,
                Images = p.Images
            });
        }
        public IEnumerable<Object> GetProductIds() {
            return getProductsFromUmbraco().Select(p => new Product {
                Id = p.Id              
            });
        }


        public Product GetProductById(int id) {

            var root = Umbraco.ContentAtRoot().First();
            var products = root.Descendants().Where(x => x.IsDocumentType("product"));


            for (var count = 1; count <= 2; ++count) {

                foreach (var prod in products) {


                    var newProduct = Services.ContentService.Create((prod.Name + "v" + count), 1076, "product");

                    newProduct.SetValue("productName", "v" + count + prod.GetProperty("productName").GetValue());
                    newProduct.SetValue("description", "v" + count + prod.GetProperty("description").GetValue());
                    newProduct.SetValue("details", "v" + count + prod.GetProperty("details").GetValue());
                    newProduct.SetValue("price", prod.GetProperty("price").GetValue());


                    Services.ContentService.SaveAndPublish(newProduct);

                }


            }

            return getProductsFromUmbraco().FirstOrDefault(p => p.Id == id);
        }

        private IEnumerable<Product> getProductsFromUmbraco() {
            var root = Umbraco.ContentAtRoot().First();
            var products = root.Descendants().Where(x => x.IsDocumentType("product"));

            ProductMapper _mapper = new ProductMapper();
            List<Product> result = new List<Product>();

            foreach (var prod in products) {
                result.Add(_mapper.MapProduct(prod));
            }

            return result;
        }
      
    }
}
