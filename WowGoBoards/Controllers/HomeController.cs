﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace WowGoBoards.Controllers
{
    public class HomeController : UmbracoController
    {
        // GET: Home
        public ActionResult Index()
        {

            try {

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = Server.MapPath("~/Content/StartNuxtServer.exe");
                process.Start();
                process.Close();
                ViewBag.Result = "Done";

            } catch ( Exception ex) {
                ViewBag.Result = ex.Message;
            }        

            return View();
        }
    }
}