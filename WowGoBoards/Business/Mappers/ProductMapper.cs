﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using WowGoBoards.Models;
using Umbraco.Web;

namespace WowGoBoards.Business.Mappers
{
    public class ProductMapper
    {
        internal Product MapProduct(IPublishedContent prod) {
            
            var images = (List<IPublishedContent>)prod.GetProperty("images").GetValue();
           
            return new Product {
                Id = prod.Id,
                ProductName = (string) prod.GetProperty("productName").GetValue(),
                Description = (string) prod.GetProperty("description").GetValue(),
                Details = (string)prod.GetProperty("details").GetValue(),
                Price = (decimal) prod.GetProperty("price").GetValue(),
                Images = images.Select(i=>i.GetUrl()).ToArray()
                
            };
        }
    }
}