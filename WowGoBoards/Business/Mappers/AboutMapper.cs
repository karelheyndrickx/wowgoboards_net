﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using WowGoBoards.Models;
using Umbraco.Web;

namespace WowGoBoards.Business.Mappers
{
    public class AboutMapper
    {
        internal About MapHome(IPublishedContent About) {

            return new About {
                AboutUsParagraph = (string)About.GetProperty("aboutUsParagraph").GetValue(),
                Title = (string)About.GetProperty("title").GetValue(),
            };
        }
    }
}