﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using WowGoBoards.Models;
using Umbraco.Web;

namespace WowGoBoards.Business.Mappers
{
    public class HomeMapper
    {
        internal Home MapHome(IPublishedContent home) {

            return new Home {
                IntroductionText = (string)home.GetProperty("introductionText").GetValue(),                                
            };
        }
    }
}