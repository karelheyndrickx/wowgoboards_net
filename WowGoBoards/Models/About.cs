﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WowGoBoards.Models
{
    public class About
    {
        public string Title { get; set; }
        public string AboutUsParagraph { get; set; }
    }
}